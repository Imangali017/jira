package com.example.jira.services.impl;

import com.example.jira.entities.TaskPriority;
import com.example.jira.entities.TaskStatus;
import com.example.jira.entities.Tasks;
import com.example.jira.repositories.TaskPriorityRepository;
import com.example.jira.repositories.TaskRepository;
import com.example.jira.repositories.TaskStatusRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TaskServiceImplTest {
    @InjectMocks
    private TaskServiceImpl taskService;
    @Mock
    private TaskRepository taskRepository;
    @Mock
    private TaskPriorityRepository taskPriorityRepository;
    @Mock
    private TaskStatusRepository taskStatusRepository;

    @Test
    void findAll_shouldFindEntries_whenCorrect() {
        Tasks tasks = new Tasks();
        tasks.setId(1L);
        tasks.setTaskName("test");
        Mockito.when(taskRepository.findAll()).thenReturn(List.of(tasks));
        List<Tasks> actual = taskService.findAll();
        List<Tasks> expected = List.of(tasks);
        assertThat(actual, equalTo(expected));
    }

    @Test
    void deleteTask_shouldDeleteTask_whenStatementCorrect() {
        Long id = 1L;
        Tasks tasks = new Tasks();
        tasks.setId(id);
        tasks.setTaskName("test");
        Mockito.when(taskRepository.findTasksById(id)).thenReturn(tasks);
        taskService.deleteTask(id);
        verify(taskRepository).deleteById(id);
    }

    @Test
    void deleteTask_shouldNotDeleteTask_whenStatementIncorrect() {
        Long id = 1L;
        Mockito.when(taskRepository.findTasksById(id)).thenReturn(null);
        taskService.deleteTask(id);
        verify(taskRepository, never()).deleteById(id);
    }

    @Test
    void addTask_shouldAddTask_whenStatementCorrect() {
        Tasks tasks = new Tasks();
        tasks.setId(1L);
        tasks.setTaskName("test");
        taskService.addItem(tasks);
        verify(taskRepository).save(tasks);
    }

    @Test
    void getPriority_shouldFindPriority_whenCorrect() {
        TaskPriority taskPriority = new TaskPriority();
        Long id = 1L;
        taskPriority.setId(id);
        taskPriority.setPriorityName("qwer");
        Mockito.when(taskPriorityRepository.getOne(id)).thenReturn(taskPriority);
        TaskPriority actual = taskService.getPriority(id);
        assertThat(actual, equalTo(taskPriority));
    }

    @Test
    void getAllPriority_shouldFinaAllPriority() {
        TaskPriority taskPriority = new TaskPriority();
        Long id = 1L;
        taskPriority.setId(id);
        taskPriority.setPriorityName("qwer");
        Mockito.when(taskPriorityRepository.findAll()).thenReturn(List.of(taskPriority));
        List<TaskPriority> taskPriorities = taskService.getAllPriority();
        List<TaskPriority> taskPriorities1 = List.of(taskPriority);
        assertThat(taskPriorities, equalTo(taskPriorities1));
    }

    @Test
    void getAllStatuses_shouldFindAllStatuses() {
        TaskStatus taskStatus = new TaskStatus();
        Long id = 1L;
        taskStatus.setId(id);
        taskStatus.setStatusName("qwerty");
        Mockito.when(taskStatusRepository.findAll()).thenReturn(List.of(taskStatus));
        List<TaskStatus> taskStatuses = taskService.getAllStatuses();
        List<TaskStatus> taskStatuses1 = List.of(taskStatus);
        assertThat(taskStatuses, equalTo(taskStatuses1));
    }

    @Test
    void findByName_shouldFindByName() {
        String a = "qwer";
        Tasks tasks = new Tasks();
        tasks.setTaskName(a);
        Mockito.when(taskRepository.findTasksByTaskNameContainingIgnoreCase(tasks.getTaskName())).thenReturn(tasks);
        Tasks tasks1 = taskService.findByName(a);
        assertThat(tasks1, equalTo(tasks));
    }
}