package com.example.jira.configs;

import com.example.jira.entities.AppUser;
import com.example.jira.repositories.AppUserRepository;
import com.example.jira.services.impl.AppUserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomSuccessHandler implements AuthenticationSuccessHandler {
    private final AppUserRepository userRepo;
    private final AppUserService userService;

    public CustomSuccessHandler(AppUserRepository userRepo,
                                AppUserService userService) {
        this.userRepo = userRepo;
        this.userService = userService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        String redirectUrl = null;
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String username = userDetails.getUsername();
        AppUser appUser = userRepo.findByUsername(username).orElseThrow();
        String output = userService.generateOtp(appUser);
        if ("success".equals(output))
            redirectUrl = "/login/otpVerification";

        new DefaultRedirectStrategy().sendRedirect(request, response, redirectUrl);
    }

}
