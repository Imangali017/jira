package com.example.jira.configs;

import com.example.jira.entities.AppUser;
import com.example.jira.entities.ConfirmationToken;
import com.example.jira.repositories.AppUserRepository;
import com.example.jira.services.EmailService;
import com.example.jira.services.RegistrationService;
import com.example.jira.services.impl.ConfirmationTokenService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@Component
public class CustomSuccessHandlerActivate implements AuthenticationSuccessHandler {

    private final AppUserRepository userRepository;
    private final EmailService emailService;
    private final RegistrationService registrationService;
    private final ConfirmationTokenService confirmationTokenService;

    public CustomSuccessHandlerActivate(AppUserRepository userRepo,
                                        EmailService emailService,
                                        RegistrationService registrationService,
                                        ConfirmationTokenService confirmationTokenService) {
        this.userRepository = userRepo;
        this.emailService = emailService;
        this.registrationService = registrationService;
        this.confirmationTokenService = confirmationTokenService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        String redirectUrl;
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String username = userDetails.getUsername();
        AppUser appUser = userRepository.findByUsername(username).orElseThrow();
        String token = UUID.randomUUID().toString();
        String link = "http://localhost:8888/api/v1/registration/enable?token=" + token;
        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                appUser
        );
        confirmationTokenService.saveConfirmationToken(confirmationToken);
        emailService.send(appUser.getEmail(), registrationService.buildEmail(appUser.getFirstName(), link));
        redirectUrl = "/login/activate";
        new DefaultRedirectStrategy().sendRedirect(request, response, redirectUrl);
    }
}