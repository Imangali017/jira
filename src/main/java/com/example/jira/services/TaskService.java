package com.example.jira.services;

import com.example.jira.entities.Messages;
import com.example.jira.entities.TaskPriority;
import com.example.jira.entities.TaskStatus;
import com.example.jira.entities.Tasks;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface TaskService {
    List<Tasks> findAll();

    List<Tasks> findByExecutor(String username);

    void addItem(Tasks tasks);

    TaskPriority getPriority(Long id);

    void deleteTask(Long id);

    List<TaskPriority> getAllPriority();

    List<TaskStatus> getAllStatuses();

    Optional<TaskStatus> getTaskStatus(Long id);

    Tasks getTaskById(Long id);

    Tasks findByName(String name);

    Page<Tasks> getTasksByPaginateAndFilter(int pageno, int pageSize, Long statusId, Long priorityId, Long executorId, String name);

    Page<Messages> getAllMessagesByPaginate(int currentPage, int size);

    Optional<Messages> getMessageById(Long id);

    void saveMessage(Messages messages);

    Page<Tasks> getTasksByPaginate(int currentPage, int size);

    Page<Tasks> getTasksByPaginateWithName(int currentPage, int size, String name);
}
