package com.example.jira.services;

import org.springframework.scheduling.annotation.Async;

public interface EmailService {
    void send(String to, String email);

    @Async
    void sendToModerator(String to, String email);

    @Async
    void sendToUser(String to, String email);
}
