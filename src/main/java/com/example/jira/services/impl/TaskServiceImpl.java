package com.example.jira.services.impl;

import com.example.jira.entities.Messages;
import com.example.jira.entities.TaskPriority;
import com.example.jira.entities.TaskStatus;
import com.example.jira.entities.Tasks;
import com.example.jira.repositories.MessagesRepository;
import com.example.jira.repositories.TaskPriorityRepository;
import com.example.jira.repositories.TaskRepository;
import com.example.jira.repositories.TaskStatusRepository;
import com.example.jira.services.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final TaskPriorityRepository taskPriorityRepository;
    private final TaskStatusRepository taskStatusRepository;
    private final MessagesRepository messagesRepository;

    public TaskServiceImpl(TaskRepository taskRepository, TaskPriorityRepository taskPriorityRepository,
                           TaskStatusRepository taskStatusRepository,
                           MessagesRepository messagesRepository) {
        this.taskRepository = taskRepository;
        this.taskPriorityRepository = taskPriorityRepository;
        this.taskStatusRepository = taskStatusRepository;
        this.messagesRepository = messagesRepository;
    }

    @Override
    public List<Tasks> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Tasks> findByExecutor(String username) {
        return taskRepository.findTasksByTaskExecutor(username);
    }

    @Override
    public void addItem(Tasks tasks) {
        taskRepository.save(tasks);
    }

    @Override
    public TaskPriority getPriority(Long id) {
        return taskPriorityRepository.getOne(id);
    }

    @Scheduled(cron = "0 0 8 * * *")
    public void addToExcel() throws IOException {
        List<Tasks> tasks = taskRepository.findObjectsBefore3Years();
        if (! tasks.isEmpty()) {
            log.info("starting scheduler");
            File file = new File("C:\\Users\\imang\\IdeaProjects\\jira\\src\\main\\resources\\excel.xlsx");
            FileInputStream inputStream = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            Sheet sheet = workbook.getSheetAt(0);
            int rowCount = sheet.getLastRowNum();
            Row row;
            Cell cell;
//            cell = row.createCell(0);
//            cell.setCellValue("Task id");
//            sheet.setColumnWidth(0,5000);
//            cell = row.createCell(1);
//            cell.setCellValue("Task name");
//            sheet.setColumnWidth(1,5000);
//            cell = row.createCell(2);
//            cell.setCellValue("Task description");
//            sheet.setColumnWidth(2,5000);
//            cell = row.createCell(3);
//            cell.setCellValue("Task deadline");
//            sheet.setColumnWidth(3,5000);
//            cell = row.createCell(4);
//            cell.setCellValue("Task task executor");
//            sheet.setColumnWidth(4,5000);
//            cell = row.createCell(5);
//            cell.setCellValue("Task status");
//            sheet.setColumnWidth(5,5000);
//            cell = row.createCell(6);
//            cell.setCellValue("Task priorities");
//            sheet.setColumnWidth(6,5000);
            for (Tasks tasks1 : tasks) {
                rowCount++;
                row = sheet.createRow(rowCount);
                cell = row.createCell(0);
                cell.setCellValue(tasks1.getId());
                cell = row.createCell(1);
                cell.setCellValue(tasks1.getTaskName());
                cell = row.createCell(2);
                cell.setCellValue(tasks1.getTaskDescription());
                cell = row.createCell(3);
                cell.setCellValue(tasks1.getTaskDeadline());
                cell = row.createCell(4);
                cell.setCellValue(tasks1.getTaskExecutor().getFirstName());
                cell = row.createCell(5);
                cell.setCellValue(tasks1.getTaskStatuses().getStatusName());
                cell = row.createCell(6);
                cell.setCellValue(tasks1.getTaskPriorities().getPriorityName());
            }
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            outputStream.close();
            taskRepository.deleteAll(tasks);
        }
    }

    @Override
    public void deleteTask(Long id) {
        Tasks tasks = taskRepository.findTasksById(id);
        if (tasks != null) {
            taskRepository.deleteById(id);
        }
    }

    @Override
    public List<TaskPriority> getAllPriority() {
        return taskPriorityRepository.findAll();
    }

    @Override
    public List<TaskStatus> getAllStatuses() {
        return taskStatusRepository.findAll();
    }

    @Override
    public Optional<TaskStatus> getTaskStatus(Long id) {
        return taskStatusRepository.findById(id);
    }

    @Override
    public Tasks getTaskById(Long id) {
        return taskRepository.findTasksById(id);
    }

    @Override
    public Tasks findByName(String name) {
        return taskRepository.findTasksByTaskNameContainingIgnoreCase(name);
    }

    @Override
    public Page<Tasks> getTasksByPaginateAndFilter(int pageno, int pageSize, Long statusId, Long priorityId, Long executorId, String name) {
        Pageable pageable = PageRequest.of(pageno, pageSize);
        return taskRepository.findTasksByTaskStatusesAndTaskPrioritiesAndTaskExecutorWithPagination(statusId, priorityId, executorId, name, pageable);
    }

    @Override
    public Page<Messages> getAllMessagesByPaginate(int currentPage, int size) {
        Pageable p = PageRequest.of(currentPage, size);
        return messagesRepository.findAll(p);
    }

    @Override
    public Optional<Messages> getMessageById(Long id) {
        return messagesRepository.findById(id);
    }

    @Override
    public void saveMessage(Messages messages) {
        messagesRepository.save(messages);
    }

    @Override
    public Page<Tasks> getTasksByPaginate(int currentPage, int size) {
        Pageable p = PageRequest.of(currentPage, size);
        return taskRepository.findAll(p);
    }

    @Override
    public Page<Tasks> getTasksByPaginateWithName(int currentPage, int size, String name) {
        Pageable p = PageRequest.of(currentPage, size);
        return taskRepository.findTasksByTaskNameContainsIgnoreCase(name,p);
    }
}