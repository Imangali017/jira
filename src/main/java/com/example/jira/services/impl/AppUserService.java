package com.example.jira.services.impl;

import com.example.jira.entities.AppUser;
import com.example.jira.entities.ConfirmationToken;
import com.example.jira.entities.enums.AppUserRole;
import com.example.jira.repositories.AppUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AppUserService implements UserDetailsService {

    private final static String USER_NOT_FOUND_MSG = "user with username %s not found";
    private final AppUserRepository appUserRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ConfirmationTokenService confirmationTokenService;
    private final JavaMailSender mailSender;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        return appUserRepository.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException(String.format(USER_NOT_FOUND_MSG, username)));
    }

    public String signUpUser(AppUser appUser) {
        boolean userExists = appUserRepository.findByUsername(appUser.getUsername()).isPresent();
        if (userExists) {
            throw new IllegalStateException("email already taken");
        }
        String encodedPassword = bCryptPasswordEncoder.encode(appUser.getPassword());
        appUser.setPassword(encodedPassword);
        appUserRepository.save(appUser);
        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                appUser
        );
        confirmationTokenService.saveConfirmationToken(confirmationToken);
        return token;
    }

    public String generateOtp(AppUser user) {
        try {
            int randomPIN = (int) (Math.random() * 9000) + 1000;
            user.setOtp(randomPIN);
            appUserRepository.save(user);
            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setFrom("imangalibolatbek@gmail.com");
            msg.setTo(user.getEmail());
            msg.setSubject("Welcome To Our Project");
            msg.setText("Hello \n\n" + "Your Login OTP :" + randomPIN + ".Please Verify. \n\n" + "Regards \n" + "ABC");
            mailSender.send(msg);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

    public String changeProfile(AppUser appUser) {
        appUserRepository.save(appUser);
        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                appUser
        );
        confirmationTokenService.saveConfirmationToken(confirmationToken);
        return token;
    }

    @Scheduled(cron = "@hourly")
    public void enableLoginMethod() {
        List<AppUser> appUsers = appUserRepository.findAll();
        for (AppUser a : appUsers) a.setEnableLogin(false);
        System.out.println("scheduler");
    }

    public void enableAppUser(String email) {
        appUserRepository.enableAppUser(email);
    }

    public void enableLogin(String email,Boolean bool) {
        appUserRepository.enableAppUserLogin(email,bool);
    }

    public List<AppUser> getAllUsers() {
        return appUserRepository.findAllByWhereRoleUser();
    }

    public Optional<AppUser> getUserById(Long id) {
        return appUserRepository.findById(id);
    }

    public void saveUser(AppUser appUser) {
        appUserRepository.save(appUser);
    }

    public Optional<AppUser> getUserByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    public Optional<AppUser> getUserModerator() {
        return Optional.ofNullable(appUserRepository.findFirstByAppUserRoleEquals(AppUserRole.ROLE_MODERATOR));
    }

    public AppUser getUserByEmail(String email) {
        return appUserRepository.findByEmail(email);
    }

    public Optional<AppUser> getUserExecutor(Long executorId) {
        return Optional.of(appUserRepository.getOne(executorId));
    }

    public Page<AppUser> getUsersByPaginate(int currentPage, int size) {
        Pageable p = PageRequest.of(currentPage, size);
        return appUserRepository.findAll(p);
    }

    public Optional<AppUser> findByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    public void forgotPassword(String email) {
        AppUser appUser = appUserRepository.findByEmail(email);
        appUser.setPassword(bCryptPasswordEncoder.encode(appUser.getForgotPassword()));
        appUserRepository.save(appUser);
    }
}
