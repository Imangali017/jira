package com.example.jira.services;

import org.springframework.transaction.annotation.Transactional;

public interface RegistrationService {
    String register(String email, String password, String username, String firstName, String lastName, String phoneNumber);

    void changeProfile(String email, String username, String firstName, String lastName);

    @Transactional
    void confirmToken(String token);

    @Transactional
    void confirmTokenLogin(String token);

    String buildEmail(String name, String link);

    void forgotPassword(String token);
}
