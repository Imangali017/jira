package com.example.jira.controllers;

import com.example.jira.entities.Messages;
import com.example.jira.entities.Tasks;
import com.example.jira.repositories.MessagesRepository;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/excel")
public class ExcelController {

    private final MessagesRepository messagesRepository;

    @PostMapping(value = "/message")
    public void messageExcel(HttpServletResponse response) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        List<Messages> messages = messagesRepository.findAll();
        Sheet sheet = workbook.createSheet("Sheet 1");
        int rowCount = 2;
        Row row;
        row = sheet.createRow(rowCount);
        row.setRowNum(rowCount);
        Cell cell;
        cell = row.createCell(0);
        cell.setCellValue("Message id");
        sheet.setColumnWidth(0, 5000);
        cell = row.createCell(1);
        cell.setCellValue("Message");
        sheet.setColumnWidth(1, 5000);
        cell = row.createCell(2);
        cell.setCellValue("Message description");
        sheet.setColumnWidth(2, 5000);
        cell = row.createCell(3);
        cell.setCellValue("Message task executor");
        sheet.setColumnWidth(3, 5000);
        cell = row.createCell(4);
        cell.setCellValue("status");
        sheet.setColumnWidth(4, 5000);
        for (Messages messages1 : messages) {
            rowCount++;
            row = sheet.createRow(rowCount);
            cell = row.createCell(0);
            cell.setCellValue(messages1.getId());
            cell = row.createCell(1);
            cell.setCellValue(messages1.getMessage());
            cell = row.createCell(2);
            cell.setCellValue(messages1.getMessage());
            cell = row.createCell(3);
            cell.setCellValue(messages1.getUsername().getFirstName());
            cell = row.createCell(4);
            cell.setCellValue(messages1.getStatusCheck() ? "Проверено" : "Не проверено");
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=example.xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }
}