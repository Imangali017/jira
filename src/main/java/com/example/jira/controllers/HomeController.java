package com.example.jira.controllers;

import com.example.jira.configs.RequestRateLimiterAspect;
import com.example.jira.entities.AppUser;
import com.example.jira.entities.Messages;
import com.example.jira.entities.ModeratorTask;
import com.example.jira.entities.TaskPriority;
import com.example.jira.entities.TaskStatus;
import com.example.jira.entities.Tasks;
import com.example.jira.entities.enums.AppUserRole;
import com.example.jira.repositories.MessagesRepository;
import com.example.jira.repositories.ModeratorTaskRepository;
import com.example.jira.services.EmailService;
import com.example.jira.services.RegistrationService;
import com.example.jira.services.TaskService;
import com.example.jira.services.impl.AppUserService;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.hibernate.Hibernate;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequiredArgsConstructor
@Aspect
public class HomeController {
    private final TaskService taskService;
    private final AppUserService userService;
    private final RegistrationService registrationService;
    private final MessagesRepository messagesRepository;
    private final ModeratorTaskRepository moderatorTaskRepository;
    private final EmailService emailService;
    @Autowired
    private RequestRateLimiterAspect requestRateLimiterAspect;


    @PostMapping(value = "/editMessage")
    public String editMessage(@RequestParam(name = "id") Long id,
                              @RequestParam(name = "status") Boolean status) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        Messages messages = taskService.getMessageById(id).orElseThrow();
        messages.setStatusCheck(status);
        taskService.saveMessage(messages);
        return "redirect:/all-messages";
    }

    @GetMapping(value = "/")
    public String index() {
        return "main";
    }

    @GetMapping(value = "/moderator-page")
    public String moderatorPageMain(Model model) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        model.addAttribute("emp", moderatorTaskRepository.findAll());
        return "moderatorPage";
    }

    @GetMapping(value = "/editMessage/{id}")
    public String editMessagePage(@PathVariable(name = "id") Long id, Model model) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        Messages messages = taskService.getMessageById(id).orElseThrow();
        model.addAttribute("messages", messages);
        return "editMessage";
    }

    @GetMapping(value = "/enable")
    public String enable() {
        return "/enable";
    }

    @GetMapping(value = "/confirmed")
    public String confirmed() {
        return "/confirmed";
    }

    @GetMapping(value = "/all-tasks")
    public String allTasks(Model model) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        int pageno = 1;
        int pageSize = 10;
        return findPaginatedAllTasks(pageno, pageSize, model);
    }

    @GetMapping(value = "/all-tasks/page/{pageno}")
    public String findPaginatedAllTasks(@PathVariable int pageno,
                                        @RequestParam(defaultValue = "10") int pageSize,
                                        Model model) {
        Page<Tasks> empList = taskService.getTasksByPaginate(pageno - 1, pageSize);
        List<Tasks> tasks = empList.getContent();
        List<TaskPriority> taskPriorities = taskService.getAllPriority();
        List<TaskStatus> taskStatuses = taskService.getAllStatuses();
        List<AppUser> taskExecutor = userService.getAllUsers();
        model.addAttribute("emp", tasks);
        model.addAttribute("priority", taskPriorities);
        model.addAttribute("status", taskStatuses);
        model.addAttribute("executor", taskExecutor);
        model.addAttribute("currentPage", pageno);
        model.addAttribute("totalPages", empList.getTotalPages());
        model.addAttribute("totalItem", empList.getTotalElements());
        return "index";
    }

    @GetMapping(value = "/indexFilter")
    public String indexFilter(Model model,
                              @RequestParam(name = "priority", required = false) Long priorityId,
                              @RequestParam(name = "status", required = false) Long statusId,
                              @RequestParam(name = "executor", required = false) Long executorId,
                              @RequestParam(name = "name", required = false) String name,
                              @RequestParam(defaultValue = "1") int pageno,
                              @RequestParam(defaultValue = "10") int pageSize) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        PolicyFactory sanitizer = new HtmlPolicyBuilder().toFactory();
        name=sanitizer.sanitize(name);
        Page<Tasks> empList = taskService.getTasksByPaginateWithName(pageno - 1, pageSize, name);
        List<Tasks> tasks = empList.getContent();
        List<TaskPriority> taskPriorities = taskService.getAllPriority();
        List<TaskStatus> taskStatuses = taskService.getAllStatuses();
        List<AppUser> taskExecutor = userService.getAllUsers();
        model.addAttribute("emp", tasks);
        model.addAttribute("priority", taskPriorities);
        model.addAttribute("status", taskStatuses);
        model.addAttribute("executor", taskExecutor);
        model.addAttribute("currentPage", pageno);
        model.addAttribute("totalPages", empList.getTotalPages());
        model.addAttribute("totalItem", empList.getTotalElements());
        return "index";
    }

    @GetMapping(value = "/all-users")
    public String allUsers(Model model) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        return findPaginatedAllUsers(0, model);
    }

    @GetMapping(value = "/all-users/page/{page}")
    public String findPaginatedAllUsers(@PathVariable int page, Model model) {
        Page<AppUser> empList = userService.getUsersByPaginate(page, 10);
        model.addAttribute("user", empList);
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", empList.getTotalPages());
        model.addAttribute("totalItem", empList.getTotalElements());
        return "allUsers";
    }

    @GetMapping(value = "/all-messages")
    public String allMessages(Model model) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        return findPaginatedAllMessages(0, model);
    }

    @GetMapping(value = "/all-messages/page/{page}")
    public String findPaginatedAllMessages(@PathVariable int page, Model model) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        Page<Messages> allMessages = taskService.getAllMessagesByPaginate(page, 10);
        model.addAttribute("message", allMessages);
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", allMessages.getTotalPages());
        model.addAttribute("totalItem", allMessages.getTotalElements());
        return "messages";
    }

    @GetMapping(value = "/client-page")
    public String moderatorPage() {
        return "clientPage";
    }

    @PostMapping(value = "/send-task")
    public String sendTask(@RequestParam(name = "name") String name,
                           @RequestParam(name = "task") String message) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        AppUser appUser = userService.getUserByUsername(getUserData()).orElseThrow();
        AppUser appModerator = userService.getUserModerator().orElseThrow();
        ModeratorTask moderatorTask = new ModeratorTask();
        moderatorTask.setNameTask(name);
        moderatorTask.setDescription(message);
        moderatorTask.setCreateTime(LocalDateTime.now());
        moderatorTask.setAppUser(appUser);
        moderatorTask.setStatus(false);
        moderatorTaskRepository.save(moderatorTask);
        emailService.sendToModerator(appModerator.getEmail(), "please check client tasks");
        return "redirect:/client-page";
    }

    @GetMapping(value = "/my-tasks")
    public String myTasks(Model model) throws Exception {
        requestRateLimiterAspect.limitRequestRate();
        List<Tasks> tasks = new ArrayList<>();
        List<TaskStatus> taskStatuses = taskService.getAllStatuses();
        AppUser appUser = userService.getUserByUsername(getUserData()).orElseThrow();
        if (appUser.getAppUserRole().equals(AppUserRole.ROLE_USER)) {
            tasks = taskService.findByExecutor(getUserData());
            TaskStatus statusToRemove = null;
            for (TaskStatus status : taskStatuses) {
                if (status.getStatusName().equals("DONE")) {
                    statusToRemove = status;
                    break;
                }
            }
            if (statusToRemove != null) {
                taskStatuses.remove(statusToRemove);
            }
        }
        if (appUser.getAppUserRole().equals(AppUserRole.ROLE_MAIN_USER)) {
            tasks = taskService.findAll();
        }
        model.addAttribute("status", taskStatuses);
        model.addAttribute("it", tasks);
        return "myTasks";
    }

    @GetMapping(value = "/addItem")
    public String create(Model model) {
        List<TaskPriority> taskPriorities = taskService.getAllPriority();
        List<TaskStatus> taskStatuses = taskService.getAllStatuses();
        List<AppUser> users = userService.getAllUsers();
        model.addAttribute("priority", taskPriorities);
        model.addAttribute("status", taskStatuses);
        model.addAttribute("executor", users);
        return "addItem";
    }

    @GetMapping(value = "/profile")
    public String profile(Model model) {
        String userName = getUserData();
        AppUser appUser = userService.getUserByUsername(userName).orElseThrow();
        model.addAttribute("currentUser", appUser);
        return "profile";
    }

    @GetMapping(value = "/editItem/{id}")
    public String edit(Model model, @PathVariable(name = "id") Long id) {
        List<TaskPriority> taskPriorities = taskService.getAllPriority();
        List<TaskStatus> taskStatuses = taskService.getAllStatuses();
        List<AppUser> users = userService.getAllUsers();
        Tasks tasks = taskService.getTaskById(id);
        model.addAttribute("priority", taskPriorities);
        model.addAttribute("status", taskStatuses);
        model.addAttribute("executor", users);
        model.addAttribute("tasks", tasks);
        return "editItem";
    }

    @GetMapping(value = "/editUser/{id}")
    public String editUser(Model model, @PathVariable(name = "id") Long id) {
        AppUser appUsers = userService.getUserById(id).orElseThrow();
        AppUserRole appUserRole = appUsers.getAppUserRole();
        boolean ROLE_USER, ROLE_ADMIN, ROLE_MODERATOR, ROLE_CLIENT,
                lockedTrue, lockedFalse, enabledTrue, enabledFalse;
        ROLE_USER = appUserRole == AppUserRole.ROLE_USER;
        ROLE_ADMIN = appUserRole == AppUserRole.ROLE_ADMIN;
        ROLE_MODERATOR = appUserRole == AppUserRole.ROLE_MODERATOR;
        ROLE_CLIENT = appUserRole == AppUserRole.ROLE_CLIENT;
        if (appUsers.getLocked()) {
            lockedTrue = true;
            lockedFalse = false;
        } else {
            lockedTrue = false;
            lockedFalse = true;
        }
        if (appUsers.isEnabled()) {
            enabledTrue = true;
            enabledFalse = false;
        } else {
            enabledTrue = false;
            enabledFalse = true;
        }
        model.addAttribute("enabledTrue", enabledTrue);
        model.addAttribute("enabledFalse", enabledFalse);
        model.addAttribute("lockedTrue", lockedTrue);
        model.addAttribute("lockedFalse", lockedFalse);
        model.addAttribute("role_user", ROLE_USER);
        model.addAttribute("role_admin", ROLE_ADMIN);
        model.addAttribute("role_moderator", ROLE_MODERATOR);
        model.addAttribute("role_client", ROLE_CLIENT);
        model.addAttribute("user", appUsers);
        return "editUser";
    }

    @PostMapping(value = "/editUsers")
    public String editUsers(@RequestParam(name = "id") Long id,
                            @RequestParam(name = "firstName") String firstName,
                            @RequestParam(name = "lastName") String lastName,
                            @RequestParam(name = "email") String email,
                            @RequestParam(name = "password") String password,
                            @RequestParam(name = "username") String username,
                            @RequestParam(name = "locked") Boolean locked,
                            @RequestParam(name = "enabled") Boolean enabled,
                            @RequestParam(name = "role") String role) {
        AppUser appUser = userService.getUserById(id).orElseThrow();
        appUser.setFirstName(firstName);
        appUser.setLastName(lastName);
        appUser.setEmail(email);
        appUser.setPassword(password);
        appUser.setUsername(username);
        appUser.setLocked(locked);
        appUser.setEnabled(enabled);
        appUser.setAppUserRole(AppUserRole.valueOf(role));
        userService.saveUser(appUser);
        return "redirect:/all-users";
    }


    @GetMapping(value = "/register")
    @PreAuthorize("isAnonymous()")
    public String register() {
        return "register";
    }

    @PostMapping(value = "/editItem")
    public String editItem(@RequestParam(name = "id") Long id,
                           @RequestParam(name = "task_name") String taskName,
                           @RequestParam(name = "priority") Long priorityId,
                           @RequestParam(name = "status") Long statusId,
                           @RequestParam(name = "executor") Long executorId,
                           @RequestParam(name = "task_description") String taskDescription,
                           @RequestParam(name = "task_deadline", required = false)
                           @DateTimeFormat(pattern = "yyyy-MM-dd") Date taskDeadline) {
        Tasks tasks = taskService.getTaskById(id);
        TaskPriority taskPriority = taskService.getPriority(priorityId);
        TaskStatus taskStatus = taskService.getTaskStatus(statusId).orElseThrow();
        AppUser users = userService.getUserExecutor(executorId).orElseThrow();
        AppUser appUser = userService.getUserByUsername(getUserData()).orElseThrow();
        if (tasks != null && taskPriority != null) {
            tasks.setTaskName(taskName);
            tasks.setTaskDeadline(taskDeadline);
            tasks.setTaskExecutor(users);
            tasks.setTaskPriorities(taskPriority);
            tasks.setTaskStatuses(taskStatus);
            tasks.setTaskDescription(taskDescription);
            tasks.setUpdate_date(LocalDate.now());
            tasks.setUpdate_user(appUser);
            taskService.addItem(tasks);
        }
        return "redirect:/editItem/" + id;
    }

    @PostMapping(value = "/deleteItem")
    public String deleteItem(@RequestParam(name = "id") Long id) {
        taskService.deleteTask(id);
        return "redirect:/";
    }

    @GetMapping(value = "/search")
    public String search(Model model,
                         @RequestParam(name = "name") String name) {
        Tasks tasks = taskService.findByName(name);
        model.addAttribute("it", tasks);
        return "index";
    }

    @PostMapping(value = "/addItem")
    @Transactional
    public String addItem(@RequestParam(name = "task_name") String taskName,
                          @RequestParam(name = "priority") Long priorityId,
                          @RequestParam(name = "status") Long statusId,
                          @RequestParam(name = "executor") Long executorId,
                          @RequestParam(name = "task_description") String taskDescription,
                          @RequestParam(name = "task_deadline", required = false)
                          @DateTimeFormat(pattern = "yyyy-MM-dd") Date taskDeadline) {
        TaskPriority taskPriority = taskService.getPriority(priorityId);
        TaskStatus taskStatus = taskService.getTaskStatus(statusId).orElseThrow();
        AppUser users = userService.getUserExecutor(executorId).orElseThrow();
        LocalDate createdDate = LocalDate.now();
        Tasks tasks = new Tasks();
        PolicyFactory sanitizer = new HtmlPolicyBuilder().toFactory();
        taskName=sanitizer.sanitize(taskName);
        taskDescription = sanitizer.sanitize(taskDescription);
        if (taskPriority != null) {
            tasks.setTaskName(taskName);
            tasks.setTaskDeadline(taskDeadline);
            tasks.setTaskExecutor(users);
            tasks.setTaskPriorities(taskPriority);
            tasks.setTaskStatuses(taskStatus);
            tasks.setTaskDescription(taskDescription);
            tasks.setStatusSend(false);
            tasks.setCreatedDate(createdDate);
            taskService.addItem(tasks);
            Hibernate.initialize(tasks.getTaskExecutor());
        }
        emailService.sendToUser(users.getEmail(), "Please check your task");
        return "redirect:/all-tasks";
    }

    @PostMapping(value = "/password-update")
    public String passwordUpdate(@RequestParam("oldPassword") String oldPassword,
                                 @RequestParam("newPassword") String newPassword,
                                 @RequestParam("repeatPassword") String repeatPassword) {
        if (newPassword.equals(repeatPassword)) {
            String userName = getUserData();
            AppUser currentUser = userService.getUserByUsername(userName).orElseThrow();
            final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            if (passwordEncoder.matches(oldPassword, currentUser.getPassword())) {
                currentUser.setPassword(passwordEncoder.encode(newPassword));
                userService.saveUser(currentUser);
                return "redirect:/profile";
            } else {
                return "redirect:/error";
            }
        } else {
            return "redirect:/passwordNotEqual";
        }
    }

    @GetMapping(value = "/change-password")
    public String changePassword() {
        return "changePassword";
    }

    @PostMapping(value = "/change-profile")
    public String changeProfile(@RequestParam(name = "email") String email,
                                @RequestParam(name = "firstName") String firstName,
                                @RequestParam(name = "username") String username,
                                @RequestParam(name = "lastName") String lastName) {
        registrationService.changeProfile(email, username, firstName, lastName);
        return "redirect:/profile";
    }

    @GetMapping(value = "/change-profile-page")
    public String changeProfilePage(Model model) {
        String userName = getUserData();
        AppUser appUser = userService.getUserByUsername(userName).orElseThrow();
        model.addAttribute("currentUser", appUser);
        return "profileChange";
    }

    @PostMapping(value = "/send-message")
    public String sendMessage(@RequestParam(name = "message") String message) {
        Messages messages = new Messages();
        String userName = getUserData();
        AppUser appUser = userService.getUserByUsername(userName).orElseThrow();
        messages.setMessage(message);
        messages.setSendMessageTime(LocalDateTime.now());
        messages.setUsername(appUser);
        messages.setStatusCheck(false);
        messagesRepository.save(messages);
        return "redirect:/";
    }


    @PostMapping(value = "/change-status")
    public String changeStatus(@RequestParam(name = "status") Long statusId,
                               @RequestParam(name = "id") Long id) {
        Tasks tasks = taskService.getTaskById(id);
        TaskStatus taskStatus = taskService.getTaskStatus(statusId).orElseThrow();
        tasks.setTaskStatuses(taskStatus);
        taskService.addItem(tasks);
        return "redirect:/my-tasks";
    }

    private String getUserData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }
}