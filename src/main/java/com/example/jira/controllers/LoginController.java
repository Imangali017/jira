package com.example.jira.controllers;

import com.example.jira.dto.UserLoginDTO;
import com.example.jira.entities.AppUser;
import com.example.jira.entities.ConfirmationToken;
import com.example.jira.services.EmailService;
import com.example.jira.services.RegistrationService;
import com.example.jira.services.impl.AppUserService;
import com.example.jira.services.impl.ConfirmationTokenService;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.UUID;

@Controller
@RequestMapping("/login")
public class LoginController {

    private final AppUserService appUserService;
    private final EmailService emailService;
    private final RegistrationService registrationService;
    private final ConfirmationTokenService confirmationTokenService;

    public LoginController(AppUserService appUserService,
                           EmailService emailService,
                           RegistrationService registrationService, ConfirmationTokenService confirmationTokenService) {
        this.appUserService = appUserService;
        this.emailService = emailService;
        this.registrationService = registrationService;
        this.confirmationTokenService = confirmationTokenService;
    }

    @GetMapping("/otpVerification")
    public String otpSent() {
        return "otpScreen";
    }

    @PostMapping("/otpVerification")
    public String otpVerification(@RequestParam(name = "otp") int otp, HttpServletRequest request) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        UserDetails user = (UserDetails) securityContext.getAuthentication().getPrincipal();
        AppUser users = appUserService.findByUsername(user.getUsername()).orElseThrow();
        if (users.getOtp() == otp && ! users.getLocked()) {
            System.out.println("otp equal");
            return "redirect:/profile";
        } else {
            System.out.println("otp no equal");
            if (users.getCountOtp() != null) {
                if (users.getCountOtp() == 3) {
                    users.setLocked(true);
                    appUserService.saveUser(users);
                    SecurityContextHolder.clearContext();
                    request.getSession().invalidate();
                    return "redirect:/login";
                }
                users.setCountOtp(users.getCountOtp() + 1);
            } else {
                users.setCountOtp(1);
            }
            appUserService.saveUser(users);
            return "redirect:/login/otpVerification?error";
        }
    }

    @PostMapping("/activateEmail")
    public String activateWithEmail(HttpServletRequest request) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        UserDetails user = (UserDetails) securityContext.getAuthentication().getPrincipal();
        AppUser users = appUserService.findByUsername(user.getUsername()).orElseThrow();
        if (users.getEnableLogin()) {
            appUserService.enableLogin(users.getEmail(), false);
            return "redirect:/profile";
        } else {
            if (users.getCountOtp() != null) {
                if (users.getCountOtp() == 3) {
                    users.setLocked(true);
                    appUserService.saveUser(users);
                    SecurityContextHolder.clearContext();
                    request.getSession().invalidate();
                    return "redirect:/login";
                }
                users.setCountOtp(users.getCountOtp() + 1);
                appUserService.saveUser(users);
            } else {
                users.setCountOtp(1);
            }
            appUserService.saveUser(users);
            return "redirect:/login/activate?error";
        }
    }


    @GetMapping("/activate")
    public String activate() {
        return "activate";
    }

    @ModelAttribute("user")
    public UserLoginDTO userLoginDTO() {
        return new UserLoginDTO();
    }

    @GetMapping
    public String login() {
        return "login";
    }

    @GetMapping(value = "/error")
    public String error() {
        return "403";
    }

    @PostMapping
    public void loginUser(@ModelAttribute("user")
                          UserLoginDTO userLoginDTO) {
        System.out.println("UserDTO" + userLoginDTO);
        appUserService.loadUserByUsername(userLoginDTO.getUsername());
    }

    @PostMapping("/activate")
    public void loginWithUser(@ModelAttribute("user")
                              UserLoginDTO userLoginDTO) {
        System.out.println("UserDTO" + userLoginDTO);
        appUserService.loadUserByUsername(userLoginDTO.getUsername());
    }

    @GetMapping(value = "/loginActivate")
    public String loginActivate() {
        return "loginActivate";
    }

    @GetMapping(value = "/loginOtp")
    public String loginOtp() {
        return "loginOtp";
    }

    @GetMapping(value = "/forgot-password")
    public String forgotPassword() {
        return "forgotPassword";
    }

    @PostMapping(value = "/forgot-password")
    public String forgottenPassword(@RequestParam(name = "email") String email,
                                    @RequestParam(name = "password") String password,
                                    @RequestParam(name = "rePassword") String rePassword) {
        if (password.equals(rePassword)) {
            String token = UUID.randomUUID().toString();
            String link = "http://localhost:8888/api/v1/registration/forgot?token=" + token;
            AppUser userByEmail = appUserService.getUserByEmail(email);
            ConfirmationToken confirmationToken = new ConfirmationToken(
                    token,
                    LocalDateTime.now(),
                    LocalDateTime.now().plusMinutes(15),
                    userByEmail
            );
            confirmationTokenService.saveConfirmationToken(confirmationToken);
            userByEmail.setForgotPassword(password);
            appUserService.saveUser(userByEmail);
            emailService.send(email, registrationService.buildEmail(userByEmail.getFirstName(), link));
            return "redirect:/login";
        }
        return "redirect:/login?error";
    }
}