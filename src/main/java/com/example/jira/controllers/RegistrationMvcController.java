package com.example.jira.controllers;

import com.example.jira.services.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/api/v1/registration")
@AllArgsConstructor
public class RegistrationMvcController {

    private final RegistrationService registrationService;

    @PostMapping()
    public String register(@RequestParam(name = "email") String email,
                           @RequestParam(name = "password") String password,
                           @RequestParam(name = "rePassword") String rePassword,
                           @RequestParam(name = "username") String username,
                           @RequestParam(name = "firstName") String firstName,
                           @RequestParam(name = "lastName") String lastName,
                           @RequestParam(name = "phoneNumber") String phoneNumber,
                           Model model) {
        if (password.equals(rePassword)) {
            String register = registrationService.register(email, password, username, firstName, lastName, phoneNumber);
            if (register != null) {
                model.addAttribute("pay",true);
                return "register";
            }
            else {
                model.addAttribute("pap",true);
                return "register";
            }
        }
        else {
            model.addAttribute("pap",true);
            return "register";
        }
    }

    @GetMapping(value = "enable")
    public String enableLogin(@RequestParam("token") String token) {
        registrationService.confirmTokenLogin(token);
        return "/enable";
    }

    @GetMapping(value = "confirm")
    public String confirm(@RequestParam("token") String token) {
        registrationService.confirmToken(token);
        return "/confirmed";
    }

    @GetMapping(value = "/forgot")
    public String forgotPassword(@RequestParam("token") String token){
        registrationService.forgotPassword(token);
        return "/confirmed";
    }

}
