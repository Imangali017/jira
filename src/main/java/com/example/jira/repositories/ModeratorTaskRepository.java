package com.example.jira.repositories;

import com.example.jira.entities.ModeratorTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModeratorTaskRepository extends JpaRepository<ModeratorTask, Long> {

}
