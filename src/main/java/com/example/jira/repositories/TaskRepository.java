package com.example.jira.repositories;

import com.example.jira.entities.Tasks;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import org.springframework.data.domain.Pageable;
import java.time.LocalDate;
import java.util.List;
import java.time.temporal.ChronoUnit;

@Repository
@Transactional
public interface TaskRepository extends JpaRepository<Tasks, Long> {
    Tasks findTasksById(Long id);

    Tasks findTasksByTaskNameContainingIgnoreCase(String name);

    @Query(value = "select t from Tasks t " +
            "where (:status is null or t.taskStatuses.id = :status) " +
            "and (:priority is null or t.taskPriorities.id = :priority) " +
            "and (:executor is null or t.taskExecutor.id = :executor) " +
            "and (:name is null or t.taskName = :name)")
    Page<Tasks> findTasksByTaskStatusesAndTaskPrioritiesAndTaskExecutorWithPagination(
            @Param("status") Long status,
            @Param("priority") Long priority,
            @Param("executor") Long executor,
            @Param("name") String name,
            Pageable pageable);

    @Query(value = "select t from Tasks t where t.taskExecutor.username=:username")
    List<Tasks> findTasksByTaskExecutor(@Param("username") String username);

    List<Tasks> findByCreatedDateBefore(LocalDate date);

    default List<Tasks> findObjectsBefore3Years() {
        LocalDate threeYearsAgo = LocalDate.now().minus(3, ChronoUnit.YEARS);
        return findByCreatedDateBefore(threeYearsAgo);
    }

    Page<Tasks> findTasksByTaskNameContainsIgnoreCase(String name, Pageable pageable);
}