package com.example.jira.repositories;


import com.example.jira.entities.AppUser;
import com.example.jira.entities.enums.AppUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByUsername(String username);

    @Transactional
    @Modifying
    @Query("UPDATE AppUser a SET a.enabled = TRUE WHERE a.email = ?1")
    void enableAppUser(String email);

    @Query("select a from AppUser a where a.appUserRole = 'ROLE_USER'")
    List<AppUser> findAllByWhereRoleUser();

    @Query("update AppUser a set a.enableLogin = ?2 where a.email=?1")
    @Transactional
    @Modifying
    void enableAppUserLogin(String email,Boolean bool);

    AppUser findByEmail(String email);

    AppUser findFirstByAppUserRoleEquals(AppUserRole appUserRole);

}
