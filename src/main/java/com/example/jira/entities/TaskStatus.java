package com.example.jira.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "task_status")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskStatus implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "status_name")
    private String statusName;

    @Override
    public String toString() {
        return "TaskStatus{" +
                "id=" + id +
                ", statusName='" + statusName + '\'' +
                '}';
    }
}