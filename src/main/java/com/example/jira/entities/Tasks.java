package com.example.jira.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "tasks")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Tasks implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "task_name")
    private String taskName;
    @ManyToOne
    @JoinColumn(name = "task_priorities")
    private TaskPriority taskPriorities;
    @Column(name = "task_deadline")
    @Temporal(TemporalType.DATE)
    private Date taskDeadline;
    @Column(name = "task_description")
    private String taskDescription;
    @ManyToOne
    @JoinColumn(name = "task_status")
    private TaskStatus taskStatuses;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task_executor")
    private AppUser taskExecutor;
    @Column(name = "status_send")
    private Boolean statusSend;
    @Column(name = "created_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate createdDate;
    @ManyToOne(fetch = FetchType.EAGER)
    private AppUser update_user;
    private LocalDate update_date;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (! (o instanceof Tasks)) return false;
        Tasks tasks = (Tasks) o;
        return Objects.equals(getId(),
                tasks.getId()) && Objects.equals(getTaskName(),
                tasks.getTaskName()) && Objects.equals(getTaskPriorities(),
                tasks.getTaskPriorities()) && Objects.equals(getTaskDeadline(),
                tasks.getTaskDeadline()) && Objects.equals(getTaskDescription(),
                tasks.getTaskDescription()) && Objects.equals(getTaskStatuses(),
                tasks.getTaskStatuses()) && Objects.equals(getTaskExecutor(),
                tasks.getTaskExecutor()) && Objects.equals(getStatusSend(),
                tasks.getStatusSend()) && Objects.equals(getCreatedDate(),
                tasks.getCreatedDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(),
                getTaskName(),
                getTaskPriorities(),
                getTaskDeadline(),
                getTaskDescription(),
                getTaskStatuses(),
                getTaskExecutor(),
                getStatusSend(),
                getCreatedDate());
    }

    @Override
    public String toString() {
        return "Tasks{" +
                "id=" + id +
                ", taskName='" + taskName + '\'' +
                ", taskPriorities=" + taskPriorities +
                ", taskDeadline=" + taskDeadline +
                ", taskDescription='" + taskDescription + '\'' +
                ", taskStatuses=" + taskStatuses +
                ", taskExecutor=" + taskExecutor +
                ", statusSend=" + statusSend +
                ", createdDate=" + createdDate +
                '}';
    }
}