package com.example.jira.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "moderator_task")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ModeratorTask {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;
    @ManyToOne
    private AppUser appUser;
    private LocalDateTime createTime;
    private String nameTask;
    private String description;
    private Boolean status;
}
