package com.example.jira.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "task_priority")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskPriority implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "priority_name")
    private String priorityName;

    @Override
    public String toString() {
        return "TaskPriority{" +
                "id=" + id +
                ", priorityName='" + priorityName + '\'' +
                '}';
    }
}