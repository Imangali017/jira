package com.example.jira.entities.enums;

public enum AppUserRole {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_MODERATOR,
    ROLE_CLIENT,
    ROLE_MAIN_USER
}
